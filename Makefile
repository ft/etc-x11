XKBGEN = perl -Ilib tools/generate-xkb ftkeys
XCOMPOSEGEN = perl -Ilib tools/generate-xcompose
XKEYSYMSGEN = perl tools/xkeysym2perl
XKEYSYMSDEF = /usr/include/X11/keysymdef.h

all: xkb/symbols/ftsym cfg/XCompose

xkb/symbols/ftsym: tools/generate-xkb lib/FTKeyboard.pm lib/XKEYSYMS.pm
	$(XKBGEN) > $@

cfg/XCompose: tools/generate-xcompose lib/FTKeyboard.pm lib/XKEYSYMS.pm
	$(XCOMPOSEGEN) > $@

test:
	@$(XKBGEN)

xkeysyms: tools/xkeysym2perl
	$(XKEYSYMSGEN) < $(XKEYSYMSDEF) > lib/XKEYSYMS.pm

.PHONY: all test xkeysyms
