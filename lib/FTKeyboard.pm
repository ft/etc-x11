package FTKeyboard;
use strict;
use warnings;
use diagnostics;
use charnames qw{ :full };

use Exporter;
use base qw{ Exporter };
use vars qw{ @EXPORT };

@EXPORT = qw{ with_prefix
              with_compose
              with_dead
              symbol
              cp
              include
              documentation
              zip
              xorgsym
              xcompose
              gen_end
              gen_group_names
              gen_mod
              gen_mod_doc
              gen_sym_header
              gen_symbols
              gen_types };

use Encode;
use XKEYSYMS;

my ($maxlvl, $indent);

$maxlvl = 8;
$indent = 4;

# This is a comment about the levels in XKB. This keymap's documentation uses a
# different logic, which is also used in the @keymap structure. But it all gets
# compiled into the levels that XKB uses, obviously.
my @moddoc = (
    q(// Modifiers),
    q(//  1: None),
    q(//  2: Shift),
    q(//  3: ISO_Level3_Shift),
    q(//  4: Shift + ISO_Level3_Shift),
    q(//  5: ISO_Level5_Shift),
    q(//  6: Shift + ISO_Level5_Shift),
    q(//  7: ISO_Level3_Shift + ISO_Level5_Shift),
    q(//  8: Shift + ISO_Level3_Shift + ISO_Level5_Shift) );

my @levels = qw{ EIGHT_LEVEL
                 EIGHT_LEVEL
                 EIGHT_LEVEL
                 EIGHT_LEVEL };

my @groups = (
    q<Eight Level German-ish (ft)>,
    q<Simple German>,
    q<Dvorak (English)>,
    q<Russian>
);

sub indent {
    my ($times) = @_;

    printf "%" . ($indent * $times) . "s", "";
}

sub level_switchbox {
    my ($level) = @_;
    return 3 if ($level == 5);
    return 4 if ($level == 4);
    return ($level - 1);
}

sub gen_sym_string {
    my ($datref) = @_;
    my ($i, $s);

    $s = '';
    my $max = $#{ $datref };
    for $i (0 .. $max) {
        my $thing = $datref->[$i];
        if ($thing =~ m/^[0-9]+$/) {
            $s .= sprintf 'ISO_Level%d_Shift', $thing;
        } else {
            $s .= sprintf 'VoidSymbol';
        }
        $s .= sprintf ', ' unless ($i == $max);
    }

    return $s;
}

sub gen_modkey {
    my ($name) = @_;

    print "partial modifier_keys\n";
    print "xkb_symbols \"" . (lc $name) . "\" {\n";
}

sub gen_mod {
    my ($name, $mod, $sym) = @_;

    gen_modkey("$name" . "_$mod");
    indent(1);
    print qq{key.type[Group1]="EIGHT_LEVEL";\n};
    indent(1);
    print "replace key <$name> {\n";
    indent(2);
    print q{[};
    my $max = 8;
    for my $i (1..$max) {
        print q{ };
        print $sym;
        print q{,} unless ($i == $max);
    }
    print qq{ ]\n};
    indent(1);
    print "};\n";
    indent(1);
    print "modifier_map $mod { <$name> };\n";
    print "};\n\n";
}

sub symbol {
    my ($sym) = @_;
    return { forcesymbol => $sym };
}

sub gen_symbols {
    my ($keymap) = @_;
    foreach my $keynum (0 .. $#{$keymap}) {
        my @gs = (sort keys %{ $keymap->[$keynum]->{symbols} });
        indent(1);
        my $init = sprintf "key %6s { ", "<" . $keymap->[$keynum]->{name} . ">";
        print $init;
        for my $gn (0 .. $#gs) {
            my $grp = $gs[$gn];
            if ($gn > 0) {
                printf "%" . (length($init) + $indent) . "s", "";
            }
            print "symbols[Group" . ($gn + 1) . "] = [ ";
            for my $lvl (1 .. $maxlvl) {
                my $shlvl = level_switchbox($lvl);
                if (defined $keymap->[$keynum]->{symbols}->{$grp}->{$shlvl}) {
                    print xorgsym($keymap->[$keynum]->{symbols}->{$grp}->{$shlvl});
                } elsif (defined $keymap->[$keynum]->{symbols}->{Group1}
                             && defined $keymap->[$keynum]->{symbols}->{Group1}->{$shlvl}) {
                    print xorgsym($keymap->[$keynum]->{symbols}->{Group1}->{$shlvl});
                } else {
                    print "NoSymbol";
                }
                print ", " unless ($lvl == $maxlvl);
            }
            if ($gn == $#gs) {
                print " ] };\n";
            } else {
                print " ],\n";
            }
        }
    }
}

sub gen_types {
    print "\n";
    for my $i (1 .. 4) {
        indent(1);
        print "key.type[Group$i]=\"" . $levels[$i - 1] . "\";\n";
    }
    print "\n";
}

sub gen_sym_header {
    my ($name) = @_;

    print "partial alphanumeric_keys modifier_keys keypad_keys
xkb_symbols \"$name\" {\n";
}

sub gen_group_names {
    print "\n";
    for my $i (1 .. 4) {
        indent(1);
        print "name[Group$i]=\"" . $groups[$i - 1] .  "\";\n";
    }
}

sub gen_mod_doc {
    my ($i);

    for $i (0 .. $#moddoc) {
        indent(1);
        print $moddoc[$i], "\n";
    }
    print "\n";
}

sub gen_end {
    print "\n};\n";
}

sub log16 {
    my ($n) = @_;
    return log($n)/log(16);
}

sub requiredhexdigits {
    my ($n) = @_;
    return 1 if ($n == 0);
    return int(log16($n)) + 1;
}

sub x11codepoint {
    my ($cp) = @_;
    my $n = requiredhexdigits($cp);
    $n += $n%2;
    $n = ($n < 4) ? 4 : $n;
    return sprintf(q{U%0} . $n . q{X}, $cp);
}

# There are two ways keys are described with Xorg: A symbol name (like "a",
# "ampersand", "Space" or "Multi_key") or a unicode code point like "U2033"
# (note that in contrast to the usual unicode convention, the string does not
# contain a plus sign after the "U").
#
# This function takes a character of any kind and produces the according Xorg
# representation for it. For example:
#
#     xorgsym("a") → "a"
#     xorgsym("&") → "ampersand"
#     xorgsym("ä") → "adiaeresis"
#     xorgsym("♥") → "heart"
#     xorgsym("ℍ") → "U210D"
#     xorgsym("Multi_key") → "Multi_key"
#
# ...you see the pattern.
sub xorgsym {
    my ($sym) = @_;
    if (ref $sym eq q{HASH} && defined $sym->{forcesymbol}) {
        return x11codepoint(ord $sym->{forcesymbol});
    } elsif ((length $sym) == 1) {
        # Length is one, so this is some sort of character: If the xkeysym
        # table has a translation for the code point, use it. Otherwise use
        # the code point itself.
        my $cp = ord $sym;
        my $tl = xkeysym($cp);
        return (defined $tl) ? $tl->{symbol} : x11codepoint($cp);
    }
    # It's not a character, so use the symbol name verbatim.
    return $sym;
}

sub zip {
    my ($xs, $ys) = @_;
    my $rc = {};
    for my $i (0 .. $#{$xs}) {
        return $rc if (not (exists $xs->[$i] and exists $ys->[$i]));
        $rc->{$xs->[$i]} = $ys->[$i];
    }
    return $rc;
}

sub with_prefix {
    my $list = pop;
    my $prefix = [ @_ ];
    return { prefix => $prefix, mapping => $list };
}

sub with_compose {
    return with_prefix("Multi_key", @_);
}

sub with_dead {
    my $key = shift;
    return with_prefix("dead_$key", @_);
}

sub cp {
    my ($cp) = @_;
    $cp =~ s,^U(\+|),0x,;
    return chr(hex($cp));
}

sub include {
    my ($inc) = @_;
    return qq{include "$inc"\n};
}

sub documentation {
    my ($doc) = @_;
    return "\n# $doc\n\n";
}

sub xckey {
    my ($key) = @_;
    return qq{<$key>};
}

sub xcprefix {
    my ($prefix) = @_;
    return join(q{ }, map { xckey(xorgsym($_)) } @{$prefix});
}
sub composexcompose {
    my ($prefix, $key, $result) = @_;
    my $symbol = xckey(xorgsym($key));
    my $id = ord($result);
    my $xorg = xorgsym(chr($id));
    my $name = charnames::viacode($id);
    printf q{%s %s : "%s" %s # %s} . qq{\n},
        $prefix, $symbol, $result, $xorg, $name;
}

sub xcompose_compile {
    my ($entry) = @_;
    my $mapping = $entry->{mapping};
    if (ref($mapping) eq q{HASH}) {
        my $prefix = xcprefix($entry->{prefix});
        foreach my $key (sort keys %{$mapping}) {
            composexcompose($prefix, $key, $mapping->{$key});
        }
    } else {
        my $key = pop @{ $entry->{prefix} };
        my $prefix = xcprefix($entry->{prefix});
        composexcompose($prefix, $key, $mapping);
    }
}

sub xcompose {
    my ($table) = @_;
    foreach my $entry (@{ $table }) {
        if (ref($entry) eq q{HASH}) {
            xcompose_compile($entry);
        } else {
            print $entry;
        }
    }
}
